import { Component, OnInit, Input } from '@angular/core';
import { WeatherService } from "../services/weather.service"

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public itemId: any
  public details: any=[];
  public popup= false;
  public forecastDetails: any = [];
  public result:any;
  constructor(private weatherservice: WeatherService) { 
    
  }

  ngOnInit(): void {
    this.itemId = ["London,UK", "Rome,IT", "Paris,FR", "Barcelona,ES", "Amsterdam,NL"];
    this.itemId.forEach((e:any)=>{
      this.fetchvalue(e);
    })
  }

  public fetchvalue(city:any){
    this.weatherservice.getWeather(city).subscribe((data)=>{
      data.sys.sunrise = new Date(data.sys.sunrise);
      data.sys.sunset = new Date(data.sys.sunset);
      this.details.push({city,data});
    })
  }
  public fetchForecast(city:any){
    this.popup = true;
    this.weatherservice.getForecast(city).subscribe((data)=>{
      
      this.forecastDetails = data.list;
      this.result = this.forecastDetails.filter((element:any) => element.dt_txt.split(' ')[1] === '09:00:00');
    })
  }

}

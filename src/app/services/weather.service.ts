import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class WeatherService{
    constructor(  public http: HttpClient) { 
    }
    public getWeather(city:any): Observable<any> {
        return this.http.get(`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=3d8b309701a13f65b660fa2c64cdc517`)
    }
    public getForecast(city:any): Observable<any> {
        return this.http.get(`http://api.openweathermap.org/data/2.5/forecast?q=${city}&appid=3d8b309701a13f65b660fa2c64cdc517`)
    }
}